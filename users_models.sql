-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-03-2019 a las 20:50:18
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_models`
--

CREATE TABLE `users_models` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `use_nam` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `use_doc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `use_ema` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `use_pas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users_models`
--

INSERT INTO `users_models` (`id`, `use_nam`, `use_doc`, `use_ema`, `use_pas`, `created_at`, `updated_at`) VALUES
(1, 'Juan Perez', '12345', 'juan@ejemplo.com', '81dc9bdb52d04dc20036dbd8313ed055', '2019-03-06 05:00:00', '2019-03-06 15:24:53'),
(2, 'Ana Maria', '22222', 'maria@ejemplo.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL),
(3, 'Pedro', '64646464', 'pedro@ejemplo.com', '81dc9bdb52d04dc20036dbd8313ed055', '2019-03-06 13:34:14', NULL),
(5, 'admin', '3232', 'admin@ejemplo.com', '21232f297a57a5a743894a0e4a801fc3', '2019-03-06 18:37:33', NULL),
(6, 'ejemplo', '1', '1@ejemplo.com', 'c4ca4238a0b923820dcc509a6f75849b', '2019-03-06 19:01:44', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users_models`
--
ALTER TABLE `users_models`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users_models`
--
ALTER TABLE `users_models`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
