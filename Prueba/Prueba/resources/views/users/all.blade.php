
<br> <br>
        <h1 align="center"> Todos Los Usuarios  </h1>
        <hr>
        <a class="btn btn-info" href="{{route('users.create')}}"> Agregar Usuario </a>
        <br>

        <table class="table table-striped">
            <thead >    
            <tr>
                    
                    <th>Nombre </th>
                    <th>Email </th>
                    <th>Acciones </th>
                    
                </tr>
            <thead>    
                <?php foreach ($users as $row) { ?>
                 <tbody>   
                 <tr>
                    <td> <br> <?= $row->use_nam ?> </td>
                    <td> <br> <?= $row->use_ema ?> </td>
                    <td> 
                    <a class="btn btn-info" href="{{ route('users.edit',  $row->id )}}"><i class="glyphicon glyphicon-cog"> </i></a>
                    <br>  <br>
                    <form action=" {{ route('users.destroy', $row->id) }}" method="POST"> 
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger"  ><i class="glyphicon glyphicon-trash"> </i></button>
                      </form> 
                </td>
                </tr>
               <tbody> 
                <?php  }?>
        </table>
    </body>        
</html>    