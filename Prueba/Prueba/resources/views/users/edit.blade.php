
<br>

<?php foreach ($usuario as $row) { ?>

<div class="col-md-12">
    <br> <br>
        <h1 align="center"> Editar Usuario  </h1>
        <hr>

    <form method="POST" action="{{route('users.update', $row->id)}}">
    @csrf
    @method('PUT')
    <br>
     
<label> Nombre* </label>
<input type="text" class="form-control" value="{{$row->use_nam}}" name="use_nam" require>
<br>
<label> Documentos* </label>
<input type="text" class="form-control" value="{{$row->use_doc}}" name="use_doc" require>
<br>
<label> Email* </label>
<input type="email" class="form-control" value="{{$row->use_ema}}" name="use_ema" require >
<br>
<label> Contraseña* </label>
<input type="text" class="form-control" value=""   name="use_pas" require>
<br>
<button class="btn btn-info"> Enviar </button>
<form>
<?php } ?>
</div>