<?php

namespace App\Http\Middleware;

use Closure;
use App\LoginControllers;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($General == true )
        return $next($request);

        return redirect('/');
    }
}
