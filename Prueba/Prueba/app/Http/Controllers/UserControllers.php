<?php

/*
  Controlador encargado de las acciones de los usuarios 
*/

namespace App\Http\Controllers;

use App\UsersModel;
use Illuminate\Http\Request;

class UserControllers extends Controller
{
     /*
          Metodo encargado de funciones del 
          index 
    */
    public function index()
    {
        $usuarios = UsersModel::getall();
        return view('layout/header').view('users/all' ,
            ['users' => $usuarios] ).view('layout/footer') ;
    }

     /*
          Metodo encargado de insertar un 
          nuevo usuario  
    */
    public function create()
    {
        return   view('layout/header').view('users/form').view('layout/footer');
    }
    
     /*
          Metodo encargado de listar 
          los registros provenientes
          de la Db
    */
    public function show()
    {
       $usuarios = UsersModel::getall();
        return view('layout/header').view('users/all' ,
            ['users' => $usuarios] ).view('layout/footer') ;
    }

     /*
          Metodo encargado de 
          llamar una funcion del modelo
          para eliminar un registro 
          de la DB 
    */
    public function destroy($id)
    {
        UsersModel:: de($id); 
        return redirect('users/show');
    }

     /*
          Metodo encargado de 
          llamar unas vista con datos
          traidos de la DB para su
          posterior modificacion
    */
    public function edit($id)
    {
        $usuario = UsersModel:: getid($id);
        return view('layout/header').view('users/edit' ,
            ['usuario' => $usuario] ).view('layout/footer') ;
        
    }
    /*
          Metodo encargado de 
          llamar una funcion del modelo
          para modificar un registro   
          de la DB
    */
    public function update($id)
    {
         $fecha = date('Y-m-d H:i:s', time()); 	
        
        $request= [
            'use_nam' =>$_POST['use_nam'],
            'use_doc'=>$_POST['use_doc'],
            'use_ema'=>$_POST['use_ema'],
            'use_pas'=>md5($_POST['use_pas']),
            'updated_at'=> $fecha ,
        ];
        UsersModel::up($request,$id);     
        return redirect('users/show');
        
    }
    
    /*
          Metodo encargado de 
          llamar una funcion del modelo
          para crear un nuevo registro   
          de la DB
    */
    public function store(Request $request)
    {
        $fecha = date('Y-m-d H:i:s', time()); 	
        $request= [
            'use_nam' =>$_POST['use_nam'],
            'use_doc'=>$_POST['use_doc'],
            'use_ema'=>$_POST['use_ema'],
            'use_pas'=>md5($_POST['use_pas']),
            'created_at'=> $fecha ,
        ];

        UsersModel::in($request);     
        return redirect('users/show');
    }


}
