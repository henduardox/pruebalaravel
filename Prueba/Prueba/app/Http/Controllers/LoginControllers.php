<?php

/*
  Controlador encargado de las acciones del login
*/



namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsersModel;

class LoginControllers extends Controller
{

    /*
          Metodo encargado de las
          acciones del login
    */
    public function login()
    {
        return view('login'); 
    }

    /*
          Metodo encargado de 
          llamar una funcion del modelo
          que tenga como resultado
          un token de la DB
          para crear una seccion 
          no nativa de laravel

    */
    public function getlogin()
    {
        $email = $_GET['email'];
        $password=  $_GET['password'];

        $y= UsersModel:: login($email,$password);
        if ( count($y)==0 ) {
          session_start(); 
          $_SESSION['general']= "false";
             return redirect('/'); 
        }
        else 
        {
             session_start();
             $_SESSION['general'] = "true" ;
           return redirect('users/show');
        }
    }

    /*
          Metodo encargado de 
          destruir la session
    */
    public function stop()
    {
        session_start();
        $_SESSION['general'] = "false"; 
        return redirect('/'); 

    }

}
