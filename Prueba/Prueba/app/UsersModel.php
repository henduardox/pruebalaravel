<?php

/*
    @Description : Modelo de consultas a la  Tabla usuarios
*/

namespace App;

use Illuminate\Database\Eloquent\Model;
 use Illuminate\Support\Facades\DB; 

class UsersModel extends Model
{

    /*
          Metodo encargado de Dar un Token de Login  
    */
    public static function login($email , $password )
    {
        $query = DB ::table('users_models')
        ->select('use_nam')
        ->where('use_ema',$email)
        ->where ('use_pas', md5($password))
        ->get();
        return $query;
    }

    /*
          Metodo encargado de Atualizar La Data
          de users_models  
    */
    public static function up($data, $id)
    {
         DB ::table('users_models')
        ->where('id',$id)->update($data);
    }

    /*
          Metodo encargado de insertar users_models  
    */
    public static function in($data)
    {
         DB ::table('users_models')
        ->insert($data);
    }

     /*
          Metodo encargado de devolver la clave primaria 
          en base la comparacion 
    */
    public static function getid($data)
    {
        $rs = DB ::table('users_models')
        ->select('*')->where('id', $data )->get();
        return  $rs ;
    }

     /*
          Metodo encargado de devolver todos 
          los datos de  users_models  
    */
    public static function getall()
    {
         $rs = DB ::table('users_models')
        ->select('*')->get();
        return  $rs ;
    }

     /*
        Metodo encargado de Eliminar una
         row de users_models  segun su clave 
         primaria 
    */
    public static function de($data)
    {
        DB ::table('users_models')
        ->where('id', $data )->delete();
    }


}
