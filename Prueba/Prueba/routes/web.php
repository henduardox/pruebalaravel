<?php

/*
    Archivo de Rutas
*/
 use App\Http\Controllers;
use App\LoginControllers;


 

Route::resource('users', 'UserControllers');
 

Route::get('/', 'LoginControllers@login');
Route::get('/getlogin', 'LoginControllers@getlogin');
Route::get('/stop', 'LoginControllers@stop');

